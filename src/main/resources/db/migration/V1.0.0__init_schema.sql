create table addresses
(
    id     uuid         not null,
    street varchar(255) not null,
    number bigint       not null,

    constraint pk_addresses
        primary key (id)
);

create table schools
(
    id         uuid         not null,
    name       varchar(255) not null,
    address_id uuid         not null,

    constraint pk_schools
        primary key (id),

    constraint fk_schools_address
        foreign key (address_id)
            references addresses (id)
);

create table teachers
(
    id        uuid         not null,
    firstname varchar(255) not null,
    lastname  varchar(255) not null,
    school_id uuid         not null,

    constraint pk_teachers
        primary key (id),

    constraint fk_teachers_school
        foreign key (school_id)
            references schools (id)
);

create table courses
(
    id         uuid         not null,
    name       varchar(255) not null,
    school_id  uuid         not null,
    teacher_id uuid         not null,

    constraint pk_courses
        primary key (id),

    constraint fk_courses_school
        foreign key (school_id)
            references schools (id),

    constraint fk_courses_teacher
        foreign key (teacher_id)
            references teachers (id)
);

create table students
(

    id        uuid         not null,
    firstname varchar(255) not null,
    lastname  varchar(255) not null,
    school_id uuid         not null,

    constraint pk_students
        primary key (id),

    constraint fk_students_school
        foreign key (school_id)
            references schools (id)
);

create table student_courses
(
    student_id  uuid         not null,
    course_id   uuid         not null,
    score       varchar(255) not null,
    times_taken bigint       not null default 0,

    constraint pk_student_courses
        primary key (student_id, course_id)
);


insert into addresses (id, street, number)
values ('b85c6951-72ee-4e1e-ab57-c99bb66fd0bc', 'False Street', 123);

insert into schools (id, name, address_id)
values ('54af1abd-6e27-4ba6-a4d4-562c7440f550', 'Spring Field School', 'b85c6951-72ee-4e1e-ab57-c99bb66fd0bc');

insert into teachers (id, firstname, lastname, school_id)
values ('df51addd-1822-4ba4-819c-3cf004d4ddba', 'Edna', 'krabappel', '54af1abd-6e27-4ba6-a4d4-562c7440f550');

insert into courses (id, name, school_id, teacher_id)
values ('8e164d45-6be8-4670-8986-1f924dcfe1cd',
        'Mathematics',
        '54af1abd-6e27-4ba6-a4d4-562c7440f550',
        'df51addd-1822-4ba4-819c-3cf004d4ddba');

insert into courses (id, name, school_id, teacher_id)
values ('204e1c1c-eef0-4a98-8231-7ff28d4017c4',
        'History',
        '54af1abd-6e27-4ba6-a4d4-562c7440f550',
        'df51addd-1822-4ba4-819c-3cf004d4ddba');

insert into courses (id, name, school_id, teacher_id)
values ('2e8cf3d5-9e8c-45e1-b2d9-0962aaa2288c',
        'Chemistry',
        '54af1abd-6e27-4ba6-a4d4-562c7440f550',
        'df51addd-1822-4ba4-819c-3cf004d4ddba');