package com.j_paz.jpa_lab.jpalab.controllers;

import com.j_paz.jpa_lab.jpalab.entities.Student;
import com.j_paz.jpa_lab.jpalab.repositories.StudentRepository;
import com.j_paz.jpa_lab.jpalab.requests.StudentCreateRequest;
import com.j_paz.jpa_lab.jpalab.requests.StudentUpdateRequest;
import com.j_paz.jpa_lab.jpalab.responses.StudentResponse;
import com.j_paz.jpa_lab.jpalab.services.StudentService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/students")
public class StudentController {

    private final StudentService studentService;
    private final StudentRepository studentRepository;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public StudentResponse save(@RequestBody StudentCreateRequest request) {
        log.info("creating student: {}", request);
        final Student student = this.studentService.save(request);
        log.info("student created successfully: {}", student);

        return new StudentResponse(student);
    }

    @ResponseStatus(HttpStatus.ACCEPTED)
    @PutMapping(path = "/{studentId}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public StudentResponse update(@PathVariable("studentId") UUID studentId, @RequestBody StudentUpdateRequest request) {
        log.info("updating student: {}", request);
        final Student student = this.studentService.update(studentId, request);
        log.info("student updated successfully: {}", student);

        return new StudentResponse(student);
    }

    @Transactional
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public List<StudentResponse> getAll() {
        return studentRepository.findAll().stream()
                .map(StudentResponse::new)
                .collect(Collectors.toList());
    }

}
