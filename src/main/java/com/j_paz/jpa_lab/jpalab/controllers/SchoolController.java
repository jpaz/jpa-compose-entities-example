package com.j_paz.jpa_lab.jpalab.controllers;

import com.j_paz.jpa_lab.jpalab.repositories.SchoolRepository;
import com.j_paz.jpa_lab.jpalab.responses.SchoolResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/schools")
public class SchoolController {

    private final SchoolRepository schoolRepository;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public List<SchoolResponse> getAll() {
        return schoolRepository.findAll().stream()
                .map(SchoolResponse::new)
                .collect(Collectors.toList());
    }

}
