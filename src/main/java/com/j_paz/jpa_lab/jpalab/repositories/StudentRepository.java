package com.j_paz.jpa_lab.jpalab.repositories;

import com.j_paz.jpa_lab.jpalab.entities.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface StudentRepository extends JpaRepository<Student, UUID> {
}
