package com.j_paz.jpa_lab.jpalab.requests;

import java.util.Set;
import java.util.UUID;

public record StudentCreateRequest(
        String firstname,
        String lastname,
        UUID schoolId,
        Set<UUID> coursesId
) {
}
