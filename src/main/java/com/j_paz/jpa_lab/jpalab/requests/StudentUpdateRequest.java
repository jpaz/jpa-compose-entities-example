package com.j_paz.jpa_lab.jpalab.requests;

import java.util.Set;
import java.util.UUID;

public record StudentUpdateRequest(Set<UUID> coursesId) {
}
