package com.j_paz.jpa_lab.jpalab.services;

import com.j_paz.jpa_lab.jpalab.entities.Student;
import com.j_paz.jpa_lab.jpalab.requests.StudentCreateRequest;
import com.j_paz.jpa_lab.jpalab.requests.StudentUpdateRequest;

import java.util.UUID;

public interface StudentService {

    Student save(StudentCreateRequest request);

    Student update(UUID studentId, StudentUpdateRequest request);
}
