package com.j_paz.jpa_lab.jpalab.services.impl;

import com.j_paz.jpa_lab.jpalab.entities.Course;
import com.j_paz.jpa_lab.jpalab.entities.School;
import com.j_paz.jpa_lab.jpalab.entities.Student;
import com.j_paz.jpa_lab.jpalab.entities.StudentCourse;
import com.j_paz.jpa_lab.jpalab.repositories.CourseRepository;
import com.j_paz.jpa_lab.jpalab.repositories.SchoolRepository;
import com.j_paz.jpa_lab.jpalab.repositories.StudentRepository;
import com.j_paz.jpa_lab.jpalab.requests.StudentCreateRequest;
import com.j_paz.jpa_lab.jpalab.requests.StudentUpdateRequest;
import com.j_paz.jpa_lab.jpalab.services.StudentService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final CourseRepository courseRepository;
    private final SchoolRepository schoolRepository;
    private final StudentRepository studentRepository;

    @Override
    @Transactional
    public Student save(StudentCreateRequest request) {
        final School school = this.schoolRepository.getReferenceById(request.schoolId());
        final List<Course> courses = this.courseRepository.findAllById(request.coursesId());

        final Student student = new Student();
        student.setFirstname(request.firstname());
        student.setLastname(request.lastname());
        student.setSchool(school);

        final Set<StudentCourse> studentCourses = courses.stream()
                .map(course -> new StudentCourse(student, course))
                .collect(Collectors.toSet());

        student.setStudentCourses(studentCourses);

        return this.studentRepository.save(student);
    }

    @Override
    @Transactional
    public Student update(UUID studentId, StudentUpdateRequest request) {
        final Student student = this.studentRepository.getReferenceById(studentId);
        final List<Course> courses = this.courseRepository.findAllById(request.coursesId());

        final Set<StudentCourse> studentCourses = courses.stream()
                .map(course -> new StudentCourse(student, course))
                .collect(Collectors.toSet());

        student.getStudentCourses().clear();
        student.getStudentCourses().addAll(studentCourses);

        return this.studentRepository.save(student);
    }
}
