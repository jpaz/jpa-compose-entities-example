package com.j_paz.jpa_lab.jpalab.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.stream.Stream;

public enum Score {
    COURSING,
    PROMOTE,
    APPROVED,
    REPROBATE;

    @JsonValue
    public String getCode() {
        return this.name();
    }

    @JsonCreator
    public static Score getByCode(String code) {
        return Stream.of(values())
                .filter(score -> score.getCode().equalsIgnoreCase(code))
                .findFirst()
                .orElse(null);
    }
}
