package com.j_paz.jpa_lab.jpalab.responses;

import com.j_paz.jpa_lab.jpalab.entities.Address;

import java.util.UUID;

public record AddressResponse(UUID id, String street, Long number) {
    public AddressResponse(Address address) {
        this(address.getId(), address.getStreet(), address.getNumber());
    }
}
