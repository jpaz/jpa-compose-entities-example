package com.j_paz.jpa_lab.jpalab.responses;

import com.j_paz.jpa_lab.jpalab.entities.Course;

import java.util.UUID;

public record CourseResponse(UUID id, String name, TeacherResponse teacher) {
    public CourseResponse(Course course) {
        this(course.getId(), course.getName(), new TeacherResponse(course.getTeacher()));
    }
}
