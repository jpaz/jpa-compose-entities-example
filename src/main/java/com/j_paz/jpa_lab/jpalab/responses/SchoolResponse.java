package com.j_paz.jpa_lab.jpalab.responses;

import com.j_paz.jpa_lab.jpalab.entities.School;

import java.util.UUID;

public record SchoolResponse (UUID id, String name, AddressResponse address) {
    public SchoolResponse(School school) {
        this(
            school.getId(),
            school.getName(),
            new AddressResponse(school.getAddress())
        );
    }
}
