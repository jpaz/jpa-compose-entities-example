package com.j_paz.jpa_lab.jpalab.responses;

import com.j_paz.jpa_lab.jpalab.entities.Teacher;

import java.util.UUID;

public record TeacherResponse(UUID id, String firstname, String lastname) {
    public TeacherResponse(Teacher teacher) {
        this(teacher.getId(), teacher.getFirstname(), teacher.getLastname());
    }
}
