package com.j_paz.jpa_lab.jpalab.responses;

import com.j_paz.jpa_lab.jpalab.entities.Student;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public record StudentResponse(UUID id,
                              String firstname,
                              String lastname,
                              SchoolResponse school,
                              List<CourseResponse> courses
) {
    public StudentResponse(Student student) {
        this(
                student.getId(),
                student.getFirstname(),
                student.getLastname(),
                new SchoolResponse(student.getSchool()),
                student.getStudentCourses().stream().map(sc -> new CourseResponse(sc.getCourse())).collect(Collectors.toList())
        );
    }
}
